#!/bin/bash
set -e
if [ $# -lt 1 ];then
    echo Usage: $0 isoimage
    exit 1
fi
RFKILLVERSION="0.5-1+b1"
ISOFILE=$1
CUSTOMISOFILE=${ISOFILE%.iso}-custom.iso
# We first extract the ISO
rm -rf cd
mkdir cd
echo Extracting DVD image
bsdtar -C cd -xf $ISOFILE

# Find the distribution codename
CODENAME=$(ls cd/dists/|egrep -v "(stable|testing)"|head -1)
SUITE=$(ls cd/dists/|egrep "(stable|testing)"|head -1)
# Replacing the kernel: put the new images in the kernel-packages folder here, if they are available
if [ -f kernel-packages/linux-image-*deb ] && [ -f kernel-packages/linux-headers-*deb ];then
    echo Removing the old kernel...
    rm -f cd/pool/main/l/linux/linux-headers-*_amd64.deb \
       cd/pool/main/l/linux/linux-headers-*_all.deb \
       cd/pool/main/l/linux/linux-image-*_amd64.deb
    echo Putting in your new kernel...
    cp kernel-packages/*deb cd/pool/main/l/linux
else
    echo Using the existing kernel in the image...
fi
KERNELVERSION=$(dpkg-scanpackages cd/pool/main/l/linux/|awk 'BEGIN { RS = "" } /Package: linux-image-.*-iitb-/ { print $2}')
echo Using kernel $KERNELVERSION...

# Add the non-free firmware for audio and Bluetooth
echo Getting the firmware for audio and wireless...
mkdir -p ./cd/pool/non-free/f/firmware-nonfree
(cd ./cd/pool/non-free/f/firmware-nonfree && wget -c http://deb.debian.org/debian/pool/non-free/f/firmware-nonfree/firmware-intel-sound_20170823-1~bpo9+1_all.deb)
(cd ./cd/pool/non-free/f/firmware-nonfree && wget -c http://ftp.us.debian.org/debian/pool/non-free/f/firmware-nonfree/firmware-realtek_20170823-1~bpo9+1_all.deb)

# Add the rfkill package to the image
echo Adding the rfkill package
mkdir -p ./cd/pool/main/r/rfkill
(cd ./cd/pool/main/r/rfkill && wget http://deb.debian.org/debian/pool/main/r/rfkill/rfkill_${RFKILLVERSION}_amd64.deb)

# Add the firmware packags to the package index file
echo Updating indices...
mkdir -p indices
wget http://ftp.de.debian.org/debian/indices/override.${CODENAME}.main.gz -O - | zcat > indices/override
echo firmware-realtek        optional        non-free/kernel >> indices/override
echo firmware-intel-sound    optional        non-free/kernel  >> indices/override

# Add additional packages
if [ -f extra-packages/*deb ]; then
    echo Adding additional packages...
    mkdir -p cd/pool/main/e/extra
    cp extra-packages/*deb cd/pool/main/e/extra
    bash enhance-override.sh
fi


# Update the list of packages
echo Updating package lists...
(cd cd && \
dpkg-scanpackages pool/main/ > dists/${CODENAME}/main/binary-amd64/Packages && \
mkdir -p dists/${CODENAME}/non-free/binary-amd64 && \
dpkg-scanpackages pool/non-free/ > dists/${CODENAME}/non-free/binary-amd64/Packages)
sed "s/CODENAME/${CODENAME}/g;s/SUITE/${SUITE}/g" config-udeb.orig > config-udeb
sed "s/CODENAME/${CODENAME}/g;s/SUITE/${SUITE}/g" config-deb.orig > config-deb
sed "s/CODENAME/${CODENAME}/g;s/SUITE/${SUITE}/g" config-rel.orig > config-rel
apt-ftparchive generate config-udeb
apt-ftparchive generate config-deb
apt-ftparchive -c config-rel release cd/dists/${CODENAME} > cd/dists/${CODENAME}/Release

# Fix MD5 sums
echo Fixing md5sums...
cd cd; md5sum `find ! -name "md5sum.txt" ! -path "./isolinux/*" -follow -type f` > md5sum.txt; cd ..

# Add post-install scripts
echo Adding preseed and Bluetooth + sound configurations
mkdir cd/RDP
cp sound-config.tar.gz cd/RDP
cp bluetooth-config.tar.gz cd/RDP
cp late_command.sh cd/
sed "s/KERNELVERSION/$KERNELVERSION/" example-preseed.txt > preseed.cfg
sh update-preseed.sh

echo Building a new ISO
rm -f $CUSTOMISOFILE
xorriso -outdev $CUSTOMISOFILE -padding 0 -map cd / -chmod 0755 / -- -boot_image isolinux dir=/isolinux -boot_image isolinux system_area=/usr/lib/ISOLINUX/isohdpfx.bin -boot_image any next -boot_image any efi_path=boot/grub/efi.img -boot_image isolinux partition_entry=gpt_basdat
